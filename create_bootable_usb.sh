#!/bin/bash
set -e

# inspired from: https://mags.zone/help/arch-usb.html

HOSTNAME="ArchUSB"
REGION="Europe"
CITY="Prague"
LOCALELINE="en_US.UTF-8"
MOUNT_POINT="/mnt/usb"

while [ "$#" != "0" ]
do
	if [ "$1" = "--efi" ]
	then
		shift
		EFI="$1"
		shift
	elif [ "$1" = "--root" ]
	then
		shift
		ROOT="$1"
		shift
	fi
done

mkfs.fat -F32 "$EFI"
mkfs.ext4 "$ROOT"

mkdir -p "$MOUNT_POINT"
mount "$ROOT" "$MOUNT_POINT"

mkdir "${MOUNT_POINT}/boot"
mount "$EFI" "${MOUNT_POINT}/boot"

pacstrap "$MOUNT_POINT" linux linux-firmware base
genfstab -U "${MOUNT_POINT}" > "${MOUNT_POINT}/etc/fstab"

arch-chroot "${MOUNT_POINT}" ln -sf "/usr/share/zoneinfo/${REGION}/${CITY}" /etc/localtime
echo ${LOCALELINE} > "${MOUNT_POINT}/etc/locale.gen"
arch-chroot "${MOUNT_POINT}" locale-gen
echo LANG=${LOCALELINE} > "${MOUNT_POINT}/etc/locale.conf"
arch-chroot "${MOUNT_POINT}" hwclock --systohc
echo "$HOSTNAME" > /etc/hostname
echo "127.0.0.1  localhost\n::1        localhost\n127.0.1.1  ${HOSTNAME}.localdomain  ${HOSTNAME}" > ${MOUNT_POINT}//etc/hosts
arch-chroot "${MOUNT_POINT}" passwd
arch-chroot "${MOUNT_POINT}" pacman -S grub efibootmgr
arch-chroot "${MOUNT_POINT}" grub-install --target=i386-pc --recheck /dev/sdb
arch-chroot "${MOUNT_POINT}" grub-install --target=x86_64-efi --efi-directory /boot --recheck --removable

arch-chroot "${MOUNT_POINT}" grub-mkconfig -o /boot/grub/grub.cfg

sync
umount -R "${MOUNT_POINT}"

